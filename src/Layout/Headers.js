import React, { Component } from "react";
import { Layout, Menu, Row, Col, Icon } from "antd";

const { Header } = Layout;
const menus = ["account", "project", "report", "profile"];

class Headers extends Component {

  goToProfile = () => {
    this.props.history.push("/profile");
  };

  render() {
    return (
      <div>
        <Layout>
          <Header
            style={{
              padding: "0px",
              zIndex: 1,
              width: "100%"
            }}
          >
            <Row>
              <Col span={20} push={4}>
                <Menu
                  theme="dark"
                  mode="horizontal"
                  style={{ lineHeight: "64px", marginLeft:'48%'}}
                >
                  <Menu.Item key={menus[0]}>
                    <Icon type="home" theme="filled" />
                    Account Management
                  </Menu.Item>
                  <Menu.Item key={menus[1]}>
                    <Icon type="heart" theme="filled" />
                    Project
                  </Menu.Item>
                  <Menu.Item key={menus[2]}>
                    <Icon type="shopping-cart" style={{ fontSize: "17px" }} />
                    Report
                  </Menu.Item>
                  <Menu.Item key={menus[3]}>
                    <Icon type="smile" style={{ fontSize: "17px" }} onClick={this.goToProfile} />
                    My Account
                  </Menu.Item>
                </Menu>
              </Col>
              <Col span={4} pull={20}>
                <h1 style={{color:'white',marginLeft:'20px',fontSize:'30px'}}>Welcome</h1>
              </Col>
            </Row>
          </Header>
        </Layout>
      </div>
    );
  }
}

export default Headers;
