import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Main from './Main'
import Profile from './Profile'
import Project from './Project'

function Routes(){
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/msin" component={Main} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/project" component={Project} />
            </Switch>
        </div>
    );

}

export default Routes;