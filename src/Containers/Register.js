import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Register extends Component {
  render() {
    return (
      <div className="App">
        <div class="App__Aside"></div>
        <div class="App__Form">
          <div class="PageSwitcher">
            <NavLink
              exact
              to="/"
              activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign In
            </NavLink>
            <NavLink
              exact
              to="/register"
              activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign Up
            </NavLink>
          </div>

          <div className="FormTitle">
            <NavLink
              exact
              to="/"
              activeClassName="FormTitle__Link--Active"
              className="FormTitle__Link"
            >
              Sign In
            </NavLink>
            Or
            <NavLink
              to="/register"
              activeClassName="FormTitle__Link--Active"
              className="FormTitle__Link"
            >
              Sign Up
            </NavLink>
          </div>

          <div className="FormCenter">
            <form>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="email">
                  Email
                </label>
                <input
                  type="email"
                  id="email"
                  className="FormField__Input"
                  placeholder="Enter your email"
                  name="email"
                />
              </div>

              <div className="FormField">
                <label className="FormField__Label" htmlFor="password">
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  className="FormField__Input"
                  placeholder="Enter your password"
                  name="password"
                />
              </div>

              {/* <div className="FormField">
                <label className="FormField__Label" htmlFor="password">
                  Confirm Password
                </label>
                <input
                  type="password"
                  id="confirm-password"
                  className="FormField__Input"
                  placeholder="Enter your password"
                  name="confirm-password"
                />
              </div> */}

              <div className="FormField">
                <label className="FormField__CheckboxLabel">
                  <input
                    className="FormField__Checkbox"
                    type="checkbox"
                    name="hasAgreed"
                  />{" "}
                  I agree all statements in{" "}
                  <a href="#" className="FormField__TermsLink">
                    terms of service
                  </a>
                </label>
              </div>

              <div className="FormField">
                <button className="FormField__Button mr-20">Sign Up</button>
                &nbsp; &nbsp;
                <a class="FormField__Link">I'm already member</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
