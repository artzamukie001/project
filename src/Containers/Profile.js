import React, { Component } from "react";
import { Avatar } from "antd";
import { NavLink } from "react-router-dom";

class Profile extends Component {
  goToEdit = () => {
    this.props.history.push("/editprofile");
  };
  render() {
    return (
      <div
        className="App"
        style={{
          padding: "16px",
          marginLeft: 50,
          minHeight: "600px",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <div className="FormCenter">
          <Avatar size={150} style={{ marginLeft: "25%" }} />
          <form>
            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Email
              </label>
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                First Name
              </label>
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="password">
                Last Name
              </label>
            </div>

            <div className="FormField">
              <NavLink
                className="FormField__Button mr-20"
                onClick={this.goToEdit}
              >
                Edit Profile
              </NavLink>
              &nbsp; &nbsp;
              <NavLink
                className="FormField__Cancel mr-20"
                onClick={this.goToProfile}
              >
                Logout
              </NavLink>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Profile;
