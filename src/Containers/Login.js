import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { message, Modal, Button } from "antd";
import { auth } from "../firebase";

class Login extends Component {
  state = {
    isLoading: false,
    email: "",
    password: "",
    isShowModal: false,
    isLogin: false,
    imageUrl: ""
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem("user-data");
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  saveInformationUser = email => {
    localStorage.setItem(
      "user-data",
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    );
    this.setState({ isLoading: false });
    this.navigateToMainPage();
  };

  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
  };

  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  onSubmitFormLogin = e => {
    e.preventDefault();
    this.setState({ isLogin: true });
    const email = this.state.email;
    const password = this.state.password;
    const isEmailValid = this.validateEmail(email);
    if (isEmailValid) {
      auth
        .signInWithEmailAndPassword(email, password)
        .then(({ user }) => {
          this.setState({ isLogin: false });
          this.saveInformationUser(user.email);
        })
        .catch(_ => {
          this.setState({ isLogin: false });
          this.setState({ isShowModal: true });
        });
    } else {
      this.setState({ isLogin: false });
      message.error("Email or Password Invalid !!", 1);
    }
  };

  handleCancel = () => {
    this.setState({ isShowModal: false, isLogin: false });
  };

  handleOk = () => {
    this.setState({ isShowModal: false, isLogin: false });
    this.props.history.push("/register");
  };

  handleRegister = () => {
    this.props.history.push("/register");
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/main");
  };

  // goToMain = () => {
  //   this.props.history.push("/main");
  // };

  render() {
    return (
      <div className="App">
        <div class="App__Aside"></div>
        <div class="App__Form">
          <div class="PageSwitcher">
            <NavLink
              exact
              to="/"
              activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign In
            </NavLink>
            <NavLink
              exact
              to="/register"
              activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign Up
            </NavLink>
          </div>

          <div className="FormTitle">
            <NavLink
              exact
              to="/"
              activeClassName="FormTitle__Link--Active"
              className="FormTitle__Link"
            >
              Sign In
            </NavLink>
            Or
            <NavLink
              to="/register"
              activeClassName="FormTitle__Link--Active"
              className="FormTitle__Link"
            >
              Sign Up
            </NavLink>
          </div>

          <div className="FormCenter">
            <form onSubmit={this.onSubmitFormLogin}>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="email">
                  Email
                </label>
                <input
                  type="email"
                  id="email"
                  className="FormField__Input"
                  placeholder="Enter your email"
                  name="email"
                  onChange={this.onEmailChange}
                />
              </div>

              <div className="FormField">
                <label className="FormField__Label" htmlFor="password">
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  className="FormField__Input"
                  placeholder="Enter your password"
                  name="password"
                  onChange={this.onPasswordChange}
                />
              </div>

              <div className="FormField">
                {/* <NavLink
                  type="submit"
                  className="FormField__Button mr-20"
                  loading={this.state.isLogin}
                >
                  Sign In
                </NavLink> */}
                <input
                  className="FormField__Button mr-20"
                  type="submit"
                  loading={this.state.isLogin}
                />
                &nbsp; &nbsp;
                <a class="FormField__Link">Create an account</a>
              </div>
            </form>
            <Modal
              title="Something went wrong"
              visible={this.state.isShowModal}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              footer={[
                <Button key="back" onClick={this.handleCancel}>
                  No
                </Button>,
                <Button key="submit" type="primary" onClick={this.handleOk}>
                  Yes
                </Button>
              ]}
            >
              <p>
                Sorry! not found this accoung but you want to create accoung ?
              </p>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
