import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Login from './Login'
import Register from './Register'
import EditProfile from './EditProfile'
import Main from './Main'
import AddProject from './AddProject'

function Routes(){
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/editProfile" component={EditProfile} />
                <Route exact path="/addProject" component={AddProject} />
                <Route component={Main} />
                
            </Switch>
        </div>
    )

}

export default Routes