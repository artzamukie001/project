import React, { Component } from "react";
import { Input } from "antd";
import { NavLink } from "react-router-dom";

const { TextArea } = Input;

class AddProject extends Component {
  goToProject = () => {
    this.props.history.push("/project");
  };

  render() {
    return (
      <div
        className="App"
        style={{
          padding: "16px",
          minHeight: "600px",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <div className="FormCenter">
          <h1 style={{ marginLeft: "5%" }}>Add New Project</h1>

          <form>
            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Project Name :
              </label>
              <input
                type="email"
                id="email"
                className="FormField__Input"
                placeholder=""
                name="email"
              />
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Member :
              </label>
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Schedule :
              </label>
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="password">
                Descliption :
              </label>
              <TextArea row={4} />
            </div>
            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                SA :
              </label>
              <input
                type="email"
                id="email"
                className="FormField__Input"
                placeholder=""
                name="email"
              />
            </div>

            <div className="FormField">
              <NavLink
                className="FormField__Button mr-20"
                onClick={this.goToProject}
              >
                Submit
              </NavLink>
              &nbsp; &nbsp;
              <NavLink
                className="FormField__Cancel mr-20"
                onClick={this.goToProject}
              >
                Cancel
              </NavLink>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default AddProject;
