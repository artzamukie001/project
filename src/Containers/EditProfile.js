import React, { Component } from "react";
import {  Avatar } from "antd";
import {
  NavLink
} from "react-router-dom";

class EditProfile extends Component {
  goToProfile= () => {
    this.props.history.push("/profile")
  };
  render() {
    return (
      <div className="App"
        style={{
          padding: "16px",
          marginLeft: 50,
          minHeight: "600px",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <div className="FormCenter">
          <h1 style={{marginLeft:'20%'}}>Edit Profile</h1>
          <Avatar size={150} style={{marginLeft:'25%'}}/>
          <form>
            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Email
              </label>
              <input
                type="email"
                id="email"
                className="FormField__Input"
                placeholder="Chang your name"
                name="email"
              />
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="email">
                Email
              </label>
              <input
                type="email"
                id="email"
                className="FormField__Input"
                placeholder="Chang your name"
                name="email"
              />
            </div>

            <div className="FormField">
              <label className="FormField__Label2" htmlFor="password">
                Password
              </label>
              <input
                type="password"
                id="password"
                className="FormField__Input"
                placeholder="Enter your password"
                name="password"
              />
            </div>

            <div className="FormField">
              <NavLink
                className="FormField__Button mr-20"
                onClick={this.goToProfile}
              >
                Submit
              </NavLink>
              &nbsp; &nbsp;
              <NavLink
                className="FormField__Cancel mr-20"
                onClick={this.goToProfile}
              >
                Cancel
              </NavLink>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default EditProfile;
