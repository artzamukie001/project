import React, { Component } from "react";
import { Input, Button, Breadcrumb, Table } from "antd";
import { NavLink } from "react-router-dom";

const { Search } = Input;
const columns = [
  {
    title: "ID",
    dataIndex: "id"
  },
  {
    title: "Project Name",
    dataIndex: "email"
  },
  {
    title: "Create By",
    dataIndex: "password"
  },
  {
    title: "Action",
    dataIndex: "",
    key: "x",
    render: () => <Button type="danger">Delete</Button>
  }
];

class Project extends Component {
  goToAddPro = () => {
    this.props.history.push("/addProject");
  };

  render() {
    return (
      <div className="FormCenter">
        <div style={{ marginLeft: "75%" }}>
          <div className="FormField">
            <Search
              placeholder="input search text"
              enterButton
              size="large"
              onSearch={value => console.log(value)}
            />
          </div>

          <div className="FormField">
            <NavLink
              className="FormField__Button mr-20"
              onClick={this.goToAddPro}
            >
              +
            </NavLink>
          </div>
        </div>
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>My Project</Breadcrumb.Item>
        </Breadcrumb>
        <div style={{ background: "#fff", padding: 24, minHeight: 380 }}>
          <h1>Project</h1>
          <Table columns={columns} />
        </div>
      </div>
    );
  }
}

export default Project;
