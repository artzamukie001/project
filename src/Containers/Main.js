import React, { Component } from "react";
import { Layout, Menu, Icon, Row, Col } from "antd";
import RoteMenu from "./RouteMenu";

const { Header, Content, Footer } = Layout;
const menus = ["main", "project", "cart", "profile"];

class Main extends Component {
  state = {
    pathName: menus[0]
  };

  // componentDidMount() {
  //   const { current } = this.props.location;
  //   let pathName = menus[0];
  //   if (current != '/main'){
  //     pathName = current.replace('/main','');
  //     if(!menus.includes(pathName)) pathName = menus[0];
  //   }
  //   this.setState({ pathName })
  // }

  onMenuClick = e => {
    var path = "/";
    if (e.key !== "") {
      path = `/${e.key}`;
    }
    this.props.history.replace(path);
    this.setState({ pathName: e.key });
  };

  goToLogin = () => {
    this.props.history.push("/");
  };

  render() {
    return (
      <div>
        <Layout>
          <Header
            style={{
              padding: "0px",
              zIndex: 1,
              width: "100%"
            }}
          >
            <Row>
              <Col span={20} push={4}>
                <Menu
                  theme="dark"
                  mode="horizontal"
                  style={{ lineHeight: "64px", marginLeft: "48%" }}
                  selectedKeys={[this.state.pathName]}
                  onClick={e => {
                    this.onMenuClick(e);
                  }}
                >
                  <Menu.Item key={menus[0]}>
                    <Icon type="home" theme="filled" />
                    Account Management
                  </Menu.Item>
                  <Menu.Item key={menus[1]}>
                    <Icon type="heart" theme="filled" />
                    Project
                  </Menu.Item>
                  <Menu.Item key={menus[2]}>
                    <Icon type="shopping-cart" style={{ fontSize: "17px" }} />
                    Report
                  </Menu.Item>
                  <Menu.Item key={menus[3]}>
                    <Icon type="smile" style={{ fontSize: "17px" }} />
                    My Account
                  </Menu.Item>
                </Menu>
              </Col>
              <Col span={4} pull={20}>
                <h1
                  style={{
                    color: "white",
                    marginLeft: "20px",
                    fontSize: "30px"
                  }}
                >
                  Welcome
                </h1>
              </Col>
            </Row>
          </Header>

          <Content style={{ padding: "0 50px", marginTop: 64 }}>
            <RoteMenu />
          </Content>

          <Footer style={{ textAlign: "center" }}>
            Project Ant Design ©2019 Created by ART
          </Footer>
        </Layout>
      </div>
    );
  }
}

export default Main;
