import firebase from "firebase/app";
import "firebase/app";
import "firebase/database";
import "firebase/auth";

var config = {
  apiKey: "AIzaSyC3IYu7eIWmdW19xajsgZjtBUKo2FG1Lig",
  authDomain: "workshop-project-dv-b34be.firebaseapp.com",
  databaseURL: "https://workshop-project-dv-b34be.firebaseio.com",
  projectId: "workshop-project-dv-b34be",
  storageBucket: "workshop-project-dv-b34be.appspot.com",
  messagingSenderId: "347020397908"
};
firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider };
